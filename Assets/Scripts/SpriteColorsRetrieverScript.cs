﻿using UnityEngine;

namespace Scripts
{
    public class SpriteColorsRetrieverScript : MonoBehaviour
    {
        #region Vars
        private Color _initColor;
        private Color _secColor;
        private Color _thrdColor;
        private Color _frthColor;
        private Color _fifthColor;
        private Color _finalColor = new Color(1, 1, 1, 1);
        private Vector4 _totalDiff;
        private Color _redMiddleColor = new Color(1f, 0.07075471f, 0.2347393f);
        private Color _blueMiddleColor = new Color(0, 0.04310322f, 1);
        private Color _greenMiddleColor = new Color(0.1401745f, 0.6603774f, 0.3741702f);
        private Color _grayMiddleColor = new Color(0.5490196f, 0.509804f, 0.509804f, 1);
        #endregion
        #region Properties
        public Color SecColor { get => _secColor; set => _secColor = value; }
        public Color ThrdColor { get => _thrdColor; set => _thrdColor = value; }
        public Color FrthColor { get => _frthColor; set => _frthColor = value; }
        public Color FifthColor { get => _fifthColor; set => _fifthColor = value; }
        public Color InitColor { get => _initColor; set => _initColor = value; }
        #endregion
        private void Awake()
        {

            switch (LevelCountsContainer.CurrentStage)
            {
                case 1:
                    if (LevelCountsContainer.CurrentLevel < 4)
                    {
                        GetComponentInChildren<SpriteRenderer>().color = _grayMiddleColor;
                    }
                    InitColor = _grayMiddleColor;
                    break;
                case 2:
                    if (LevelCountsContainer.CurrentLevel < 4)
                    {
                        GetComponentInChildren<SpriteRenderer>().color = _redMiddleColor;
                    }
                    InitColor = _redMiddleColor;
                    break;
                case 3:
                    if (LevelCountsContainer.CurrentLevel < 4)
                    {
                        GetComponentInChildren<SpriteRenderer>().color = _blueMiddleColor;
                    }
                    InitColor = _blueMiddleColor;
                    break;
                case 4:
                    if (LevelCountsContainer.CurrentLevel < 4)
                    {
                        GetComponentInChildren<SpriteRenderer>().color = _greenMiddleColor;
                    }
                    InitColor = _greenMiddleColor;
                    break;
            }

            _totalDiff = _finalColor - InitColor;
            SecColor = new Color(InitColor.r + 0.23f * _totalDiff.x, InitColor.g + 0.23f * _totalDiff.y, InitColor.b + 0.23f * _totalDiff.z, 1);
            ThrdColor = new Color(InitColor.r + 2 * 0.23f * _totalDiff.x, InitColor.g + 2 * 0.23f * _totalDiff.y, InitColor.b + 2 * 0.23f * _totalDiff.z, 1);
            FrthColor = new Color(InitColor.r + 3 * 0.23f * _totalDiff.x, InitColor.g + 3 * 0.23f * _totalDiff.y, InitColor.b + 3 * 0.23f * _totalDiff.z, 1);
            FifthColor = new Color(InitColor.r + 4 * 0.23f * _totalDiff.x, InitColor.g + 4 * 0.23f * _totalDiff.y, InitColor.b + 4 * 0.23f * _totalDiff.z, 1);
        }
    }
}