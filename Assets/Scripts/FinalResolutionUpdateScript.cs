﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class FinalResolutionUpdateScript : MonoBehaviour
    {
        private const string FinalResolution1 = "Acromatopsia";
        private const string FinalResolution2 = "Protanopia";
        private const string FinalResolution3 = "Tritanopia";
        private const string FinalResolution4 = "Deuteranopia";
        private void Awake()
        {
            ProcessFinalResolutionText();
        }

        private void ProcessFinalResolutionText()
        {
            var finalResolutionText = GetComponent<Text>();
            if(LevelCountsContainer.GrayPerc < LevelCountsContainer.CorrectAnswerThreshold)
            {
                //finalResolutionText.text = FinalResolution1;
                finalResolutionText.text = $"{finalResolutionText.text} {FinalResolution1}\n";
            }

            if (LevelCountsContainer.RedGreenProgressPerc < LevelCountsContainer.CorrectAnswerThreshold)
            {
                //finalResolutionText.text = FinalResolution2;
                finalResolutionText.text = $"{finalResolutionText.text} {FinalResolution2}\n";

            }

            if (LevelCountsContainer.PurpleBluePerc < LevelCountsContainer.CorrectAnswerThreshold)
            {
                //finalResolutionText.text = FinalResolution3;
                finalResolutionText.text = $"{finalResolutionText.text} {FinalResolution3}\n";

            }

            if (LevelCountsContainer.PurpleGreenPerc < LevelCountsContainer.CorrectAnswerThreshold)
            {
                //finalResolutionText.text = FinalResolution4;
                finalResolutionText.text = $"{finalResolutionText.text} {FinalResolution4}\n";

            }

        }
    }
}