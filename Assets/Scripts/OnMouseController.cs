﻿using UnityEngine;

namespace Scripts
{
    public class OnMouseController : MonoBehaviour
    {
        private GameManager gameManager;

        public void OnMouseDown()
        {
            gameManager.ChangeStage(gameObject);
            //Suggerencia Xavi: fer cambi color quan fa click

            Debug.Log($"Current Lvl: {LevelCountsContainer.CurrentLevel}");
            Debug.Log($"Current Stage: {LevelCountsContainer.CurrentStage}");
            //Debug.Log($"Gray%: {LevelCountsContainer.GrayPerc}");
            //Debug.Log($"RedGreen%: {LevelCountsContainer.RedGreenProgressPerc}");
            //Debug.Log($"PurpleBlue%: {LevelCountsContainer.PurpleBluePerc}");
            //Debug.Log($"PurpleGreen%: {LevelCountsContainer.PurpleGreenPerc}");
        }

        void OnEnable()
        {
            gameManager = GetComponentInParent<GameManager>();
        }
    }
}