﻿using System.Collections;
using UnityEngine;

public class SpritesColorTestScript : MonoBehaviour
{
    private SpriteRenderer _spriteRend;
    private Color _finalColor = new Color(1, 1, 1, 1);
    private Color _halfColor = new Color(0.9905f, 0.7075f, 0.6135f, 1);

    private Color _initColor;
    private Color _secColor;
    private Color _thrdColor;
    private Color _frthColor;
    private Color _fifthColor;

    private Color[] colors = new Color[5];
    private Vector4 _totalDiff;

    private int i = -1;
    void Start()
    {
        _spriteRend = GetComponent<SpriteRenderer>();
        _initColor = _spriteRend.color;

        Debug.Log("Color TEST: ");
        Debug.Log($"Init color: {_initColor}");
        _totalDiff = _finalColor - _initColor;

        _secColor = new Color(_initColor.r + 0.167f * _totalDiff.x, _initColor.g + 0.167f * _totalDiff.y, _initColor.b + 0.167f * _totalDiff.z, 1);
        Debug.Log($"Sec color: {_secColor}");
        _thrdColor = new Color(_initColor.r + 2 * 0.167f * _totalDiff.x, _initColor.g + 2 * 0.167f * _totalDiff.y, _initColor.b + 2 * 0.167f * _totalDiff.z, 1);
        Debug.Log($"Thr color: {_thrdColor}");
        _frthColor = new Color(_initColor.r + 3 * 0.167f * _totalDiff.x, _initColor.g + 3* 0.167f * _totalDiff.y, _initColor.b + 3 * 0.167f * _totalDiff.z, 1);
        Debug.Log($"Frth color: {_frthColor}");
        _fifthColor = new Color(_initColor.r + 4 * 0.167f * _totalDiff.x, _initColor.g + 4 * 0.167f * _totalDiff.y, _initColor.b + 4 * 0.167f * _totalDiff.z, 1);
        Debug.Log($"Frth color: {_fifthColor}");


        colors[0] = _initColor;
        colors[1] = _secColor;
        colors[2] = _thrdColor;
        colors[3] = _frthColor;
        colors[4] = _fifthColor;


        StartCoroutine(ChangeSpriteColor(colors[1]));
    }

    IEnumerator ChangeSpriteColor(Color color)
    {
        if(i > 3)
        {
            i = 0;
        }

        else
        {
            i++;
        }

        yield return new WaitForSeconds(3);
        _spriteRend.color = color;
        StartCoroutine(ChangeSpriteColor(colors[i]));
    }
}
