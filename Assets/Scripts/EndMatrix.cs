﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public class EndMatrix : MonoBehaviour
    {
        #region Inspector
        [SerializeField] private int _width = 18;
        [SerializeField] private int _height = 18;
        [SerializeField] private Node _nodePrefab;
        [SerializeField] private Sprite _backgroundSprite;
        [SerializeField] private Sprite _centerSprite;
        #endregion
        #region Vars
        private readonly Node[] nodes = new Node[35];
        private readonly int[] arrayPosicionsParaulaEnd = new int[] { 43, 44, 45, 46, 47, 61, 63, 65, 79, 83, 115, 116, 117, 118, 119, 136, 153, 170, 187, 188, 189, 190, 191, 223, 224, 225, 226, 227, 241, 245, 259, 263, 278, 279, 280 };
        #endregion

        void Start()
        {
            EndGridGenerator();
        }

        void EndGridGenerator()
        {
            int comptadorNode = 0;
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    var node = Instantiate(_nodePrefab, new Vector2(x, y), Quaternion.identity);
                    InitNodeConfig(node);

                    for (int a = 0; a < arrayPosicionsParaulaEnd.Length; a++)
                    {
                        if (comptadorNode == arrayPosicionsParaulaEnd[a])
                        {
                            nodes[a] = node;
                        }
                    }
                    comptadorNode++;
                }
            }
            PrintEnd();
            CenterCamera();
        }

        private void InitNodeConfig(Node node)
        {
            node.transform.parent = gameObject.transform;
            node.SpriteRenderer = node.GetComponentInChildren<SpriteRenderer>();
            node.SpriteRenderer.sprite = _backgroundSprite;
            node.GetComponentInChildren<Transform>().localScale = new Vector3(1.88f, 1.88f, 1);
        }

        private void PrintEnd()
        {
            for (int i = 0; i < nodes.Length; i++)
            {
                var printingNode = nodes[i];
                printingNode.SpriteRenderer.sprite = _centerSprite;
            }
        }

        private void CenterCamera()
        {
            var center = new Vector2((float)_width / 2 - 0.5f, (float)_height / 2 - 0.5f);
            Camera.main.transform.position = new Vector3(center.x, center.y, -10);
        }
    }
}