﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Scripts
{
    public class GridGenerator : MonoBehaviour
    {
        #region Inspector
        [SerializeField] public int _width = 18;
        [SerializeField] public int _height = 18;
        [SerializeField] private Node _nodePrefab;
        [SerializeField] private Sprite[] _colorSprites = new Sprite[4];
        #endregion
        private readonly List<Node> _nodesList = new List<Node>();
        private new Color _xD = new Color(0.7159f, 0.7159f, 0.7159f);

        void Start()
        {
            GenerateGrid();
            GetRandyNode();
            EnableRandyAnimation();
        }

        private void GenerateGrid()
        {
            GenerateMatrixSetup(gameObject, new Vector2(_width, _height), new Vector2(8.5f, 8.5f));

            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    var node = Instantiate(_nodePrefab, new Vector2(x, y), Quaternion.identity);
                    InitNodeConfig(node, x, y);
                    InitNodeColor(node);
                    _nodesList.Add(node);
                }
            }
            CenterCamera();
            AnimationFix(70);
        }

        private void AnimationFix(int initialNodesDarken)
        {
            for(int i = 0; i < initialNodesDarken; i++)
            {
                _nodesList.ElementAt(Random.Range(0, _nodesList.Count)).SpriteRenderer.color = _xD;
            }
        }

        private void InitNodeColor(Node node)
        {
            switch (LevelCountsContainer.CurrentStage)
            {
                case 1:
                    node.SpriteRenderer.sprite = _colorSprites[0];
                    break;
                case 2:
                    node.SpriteRenderer.sprite = _colorSprites[1];
                    break;
                case 3:
                    node.SpriteRenderer.sprite = _colorSprites[2];
                    break;
                case 4:
                    node.SpriteRenderer.sprite = _colorSprites[3];
                    break;
            }
        }

        private void InitNodeConfig(Node node, int x, int y)
        {
            node.transform.parent = gameObject.transform;
            node.CordX = x;
            node.CordY = y;
            node.SpriteRenderer = node.GetComponentInChildren<SpriteRenderer>();
            node.GetComponentInChildren<Transform>().localScale = new Vector3(1.88f, 1.88f, 1);
        }

        private void GenerateMatrixSetup(GameObject gameObject, Vector2 size, Vector2 offset)
        {
            var collider = gameObject.AddComponent<BoxCollider2D>();
            collider.size = size;
            collider.offset = offset;

            gameObject.AddComponent<OnMouseController>();
        }

        private void CenterCamera()
        {
            var center = new Vector2((float)_width / 2 - 0.5f, (float)_height / 2 - 0.5f);
            Camera.main.transform.position = new Vector3(center.x, center.y, -10);
        }
        private void GetRandyNode()
        {
            var randomNode = _nodesList.ElementAt(Random.Range(0, _nodesList.Count));
            while (randomNode.CordX == 0 || randomNode.CordY == 0 || randomNode.CordX == (_width - 1) || randomNode.CordY == (_height - 1))
            {
                randomNode = _nodesList.ElementAt(Random.Range(0, _nodesList.Count));
            }
            GenerateCorrectZone(randomNode.CordX, randomNode.CordY);
            GenerateMatrixSetup(randomNode.gameObject, new Vector2(3.21f, 3.21f), new Vector2(0, 0));
        }

        private void GenerateCorrectZone(int cordX, int cordY)
        {
            var correctZoneNodes = new List<Node>
            {
                _nodesList.Where(node => node.CordX == cordX && node.CordY == cordY).First(),
                _nodesList.Where(node => node.CordX == cordX - 1 && node.CordY == cordY - 1).First(),
                _nodesList.Where(node => node.CordX == cordX - 1 && node.CordY == cordY).First(),
                _nodesList.Where(node => node.CordX == cordX - 1 && node.CordY == cordY + 1).First(),
                _nodesList.Where(node => node.CordX == cordX && node.CordY == cordY + 1).First(),
                _nodesList.Where(node => node.CordX == cordX + 1 && node.CordY == cordY).First(),
                _nodesList.Where(node => node.CordX == cordX + 1 && node.CordY == cordY + 1).First(),
                _nodesList.Where(node => node.CordX == cordX + 1 && node.CordY == cordY - 1).First(),
                _nodesList.Where(node => node.CordX == cordX && node.CordY == cordY - 1).First()
            };

            ColorControl(correctZoneNodes);
        }

        private void ColorControl(List<Node> correctZoneNodes)
        {
            foreach (var node in correctZoneNodes)
            {
                node.gameObject.AddComponent<SpriteColorsRetrieverScript>();

                if (LevelCountsContainer.CurrentLevel >= 4 && LevelCountsContainer.CurrentLevel < 8)
                {
                    node.SpriteRenderer.color = node.GetComponent<SpriteColorsRetrieverScript>().SecColor;
                }

                else if (LevelCountsContainer.CurrentLevel >= 8 && LevelCountsContainer.CurrentLevel < 12)
                {
                    node.SpriteRenderer.color = node.GetComponent<SpriteColorsRetrieverScript>().ThrdColor;
                }

                else if (LevelCountsContainer.CurrentLevel >= 12 && LevelCountsContainer.CurrentLevel < 16)
                {
                    node.SpriteRenderer.color = node.GetComponent<SpriteColorsRetrieverScript>().FrthColor;
                }

                else if (LevelCountsContainer.CurrentLevel >= 16)
                {
                    node.SpriteRenderer.color = node.GetComponent<SpriteColorsRetrieverScript>().FifthColor;
                }
            }
        }

        private void EnableRandyAnimation()
        {
            foreach (var node in _nodesList)
            {
                StartCoroutine(EnableAnimationRoutine(node));
            }
        }

        IEnumerator EnableAnimationRoutine(Node node)
        {
            yield return new WaitForSeconds(Random.Range(0, 1.23f));
            node.GetComponent<SpritesAnimationScript>().enabled = true;
        }
    }
}