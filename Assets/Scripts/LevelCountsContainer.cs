﻿namespace Scripts
{
    public static class LevelCountsContainer
    {
        #region Vars
        private static byte _currentStage = 1;
        private static byte _currentLevel = 1;
        private static byte _grayPerc;
        private static byte _redGreenPerc;
        private static byte _purpleBluePerc;
        private static byte _purpleGreenPerc;
        private static byte _failedTries;
        private static byte _correctAnswerThreshold = 70;
        #endregion
        #region Properties
        public static byte CurrentStage { get => _currentStage; set => _currentStage = value; }
        public static byte RedGreenProgressPerc { get => _redGreenPerc; set => _redGreenPerc = value; }
        public static byte CurrentLevel { get => _currentLevel; set => _currentLevel = value; }
        public static byte GrayPerc { get => _grayPerc; set => _grayPerc = value; }
        public static byte PurpleBluePerc { get => _purpleBluePerc; set => _purpleBluePerc = value; }
        public static byte PurpleGreenPerc { get => _purpleGreenPerc; set => _purpleGreenPerc = value; }
        public static byte FailedTries { get => _failedTries; set => _failedTries = value; }
        public static byte CorrectAnswerThreshold { get => _correctAnswerThreshold; set => _correctAnswerThreshold = value; }
        #endregion
        public static void ProcessFailedUserTries()
        {
            FailedTries++;
        }

        public static void ProcessCorrectUserTries()
        {
            if (CurrentStage == 1)
            {
                GrayPerc += 100;
                CurrentStage++;
            }

            else if (CurrentStage == 2)
            {
                _redGreenPerc += 5;
                CurrentLevel++;


                if (CurrentLevel > 20)
                {
                    CurrentStage++;
                    CurrentLevel = 1;
                }
            }

            else if (CurrentStage == 3)
            {
                PurpleBluePerc += 5;
                CurrentLevel++;

                if (CurrentLevel > 20)
                {
                    CurrentStage++;
                    CurrentLevel = 1;
                }
            }

            else if (CurrentStage == 4)
            {
                PurpleGreenPerc += 5;
                CurrentLevel++;

                if (CurrentLevel >= 20)
                {
                    CurrentStage++;
                    CurrentLevel = 1;
                }
            }

            FailedTries = 0;
        }
    }
}