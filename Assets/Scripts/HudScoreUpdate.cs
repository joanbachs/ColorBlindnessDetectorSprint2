﻿using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class HudScoreUpdate : MonoBehaviour
    {
        #region Inspector
        [SerializeField] private Text _blackWhiteText;
        [SerializeField] private Text _redGreenScoreText;
        [SerializeField] private Text _purpleBlueScoreText;
        [SerializeField] private Text _purpleGreen;
        #endregion
        private void Awake()
        {
            UpdateHudValues();
        }

        private void UpdateHudValues()
        {
            _blackWhiteText.text = $"{LevelCountsContainer.GrayPerc}%";
            _redGreenScoreText.text = $"{LevelCountsContainer.RedGreenProgressPerc}%";
            _purpleBlueScoreText.text = $"{LevelCountsContainer.PurpleBluePerc}%";
            _purpleGreen.text = $"{LevelCountsContainer.PurpleGreenPerc}%";
        }
    }
}