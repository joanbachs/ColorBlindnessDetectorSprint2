﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts
{
    public class LaunchScreenSceneController : MonoBehaviour
    {
        [SerializeField] private float _delayTime;
        private void Start()
        {
            StartCoroutine(LoadInitScreenAfterDelay());
        }

        IEnumerator LoadInitScreenAfterDelay()
        {
            yield return new WaitForSeconds(_delayTime);
            SceneManager.LoadScene(1);
        }
    }
}
