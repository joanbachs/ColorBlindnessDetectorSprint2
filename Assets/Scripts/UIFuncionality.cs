﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scripts
{
    public class UIFuncionality : MonoBehaviour
    {
        private GameManager gameManager;
        public void OnResetButtonClick()
        {
            ResetGameState();
            SceneManager.LoadScene(0);

        }

        public void OnQuitButtonClick()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
        }

        public void OnICantSeeAnythingClickButton()
        {
            LevelCountsContainer.FailedTries++;
            LevelCountsContainer.FailedTries++;
            NextStageCheck();
            
        }

        public void ResetGameState()
        {
            LevelCountsContainer.CurrentLevel = 1;
            LevelCountsContainer.CurrentStage = 1;
            LevelCountsContainer.PurpleBluePerc = 0;
            LevelCountsContainer.PurpleGreenPerc = 0;
            LevelCountsContainer.GrayPerc = 0;
            LevelCountsContainer.FailedTries = 0;
        }
        
        private void NextStageCheck()
        {
            if (LevelCountsContainer.FailedTries > 1)
            {
                LevelCountsContainer.CurrentStage++;
                LevelCountsContainer.FailedTries = 0;
                LevelCountsContainer.CurrentLevel = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
        
    }
}

