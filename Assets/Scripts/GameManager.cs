﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    public class GameManager : MonoBehaviour
    {
        #region Inspector
        [SerializeField] private Text _clickOnTheSquareText;
        [SerializeField] private GameObject _tryAgainGO;
        #endregion
        private void Awake()
        {
            FinalStageCheck();
        }
        public void ChangeStage(GameObject gameObject)
        {
            if (gameObject == this.gameObject)
            {
                LevelCountsContainer.ProcessFailedUserTries();
                TryAgainCheck();
            }

            else
            {
                LevelCountsContainer.ProcessCorrectUserTries();
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }

        }

        private void FinalStageCheck()
        {
            if(LevelCountsContainer.CurrentStage > 4)
            {
                GetComponent<GridGenerator>().enabled = false;
                GetComponent<EndMatrix>().enabled = true;
                _clickOnTheSquareText.enabled = false;
                ProcessFinalResult();
            }
        }

        private void ProcessFinalResult()
        {
            //Si todos los % son más de 70 win sinó loose
            if(LevelCountsContainer.GrayPerc > LevelCountsContainer.CorrectAnswerThreshold && LevelCountsContainer.RedGreenProgressPerc > LevelCountsContainer.CorrectAnswerThreshold && LevelCountsContainer.PurpleBluePerc > LevelCountsContainer.CorrectAnswerThreshold && LevelCountsContainer.PurpleGreenPerc > LevelCountsContainer.CorrectAnswerThreshold)
            {
                StartCoroutine(ChangeSceneDelayRoutine(2));
            }

            else
            {
                StartCoroutine(ChangeSceneDelayRoutine(3));
            }
        }

        private void TryAgainCheck()
        {
            if(LevelCountsContainer.FailedTries > 0)
            {
                _clickOnTheSquareText.enabled = false;
                _tryAgainGO.SetActive(true);
            }

            NextStageCheck();
        }

        public void NextStageCheck()
        {
            if (LevelCountsContainer.FailedTries > 1)
            {
                LevelCountsContainer.CurrentStage++;
                LevelCountsContainer.FailedTries = 0;
                LevelCountsContainer.CurrentLevel = 1;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        IEnumerator ChangeSceneDelayRoutine(int sceneIndex)
        {
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene(sceneIndex);
        }
    }
}