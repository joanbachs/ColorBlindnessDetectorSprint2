﻿using System.Collections;
using UnityEngine;

namespace Scripts
{
    public class SpritesAnimationScript : MonoBehaviour
    {
        private Node _node;
        private bool _isBrightening;
        private void Awake()
        {
            _node = GetComponent<Node>();
        }

        private void Start()
        {
            StartCoroutine(SpritesBrightnessAnimRoutine());
        }

        private void Update()
        {
            SpritesBrightNessAnimation();
        }

        public void SpritesBrightNessAnimation()
        {
            if (!_isBrightening)
            {
                _node.SpriteRenderer.color = new Color(_node.SpriteRenderer.color.r - 0.0008f, _node.SpriteRenderer.color.g - 0.0008f, _node.SpriteRenderer.color.b - 0.0008f);
            }

            else
            {
                _node.SpriteRenderer.color = new Color(_node.SpriteRenderer.color.r + 0.0008f, _node.SpriteRenderer.color.g + 0.0008f, _node.SpriteRenderer.color.b + 0.0008f);
            }
        }

        IEnumerator SpritesBrightnessAnimRoutine()
        {
            yield return new WaitForSeconds(1.05f);
            _isBrightening = true;
            yield return new WaitForSeconds(1.05f);
            _isBrightening = false;
            StartCoroutine(SpritesBrightnessAnimRoutine());
        }
    }
}