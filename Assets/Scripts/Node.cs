﻿using UnityEngine;

namespace Scripts
{
    public class Node : MonoBehaviour
    {
        private int cordX;
        private int cordY;
        private SpriteRenderer spriteRenderer;

        public int CordX { get => cordX; set => cordX = value; }
        public int CordY { get => cordY; set => cordY = value; }
        public SpriteRenderer SpriteRenderer { get => spriteRenderer; set => spriteRenderer = value; }
    }
}